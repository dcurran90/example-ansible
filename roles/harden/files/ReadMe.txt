This archive contains a shell script for configuring CentOS Linux 6 in compliance with the CIS CentOS Linux 6 benchmark v2.0.1.  It was tested against CentOS Linux 6.7 as assessed by CIS-CAT v3.0.26.

To execute the script pass a single parameter for the profile desired:

# sh CIS_CentOS_Linux_6_Benchmark_v2.0.1.sh "Level 1 - Server"

# sh CIS_CentOS_Linux_6_Benchmark_v2.0.1.sh "Level 2 - Server"

# sh CIS_CentOS_Linux_6_Benchmark_v2.0.1.sh "Level 1 - Workstation"

# sh CIS_CentOS_Linux_6_Benchmark_v2.0.1.sh "Level 2 - Workstation"

If no parameter is passed the script will default to "Level 1 - Server".


In addition to non cis-cat assessed items, the following are not configured by this script:

1.1.2 - 1.1.17
	System partitioning must be completed manually.

1.4.2 Ensure\ bootloader\ password\ is\ set
	Passwords must be set manually.

1.5.2 Ensure\ XD/NX\ support\ is\ enabled
	XD/NX support is kernel based.

1.6.1.1 - 1.6.1.3
  Modification of SELinux settings can prevent system boot, should be completed manually.

1.6.1.6
	Investigation and remediation must be completed manually

1.7.2 Ensure\ GDM\ login\ banner\ is\ configured
	GDM configuration should be configured by hand to prevent misconfiguration.

2.2.15 Ensure\ mail\ transfer\ agent\ is\ configured\ for\ local-only\ mode
	Remediation depends on MTA in use.

3.3.3 Ensure IPv6 is disabled
	Should only be disabled if not intended for use in your environment

3.4.3 Ensure\ /etc/hosts.deny\ is\ configured
	Automated configuration may lockout administration.

3.6.2 - 3.6.5
	Automated configuration may lockout administration.

4.2.1.2 - 4.2.1.5
	Log server configuration should be configured by hand to prevent misconfiguration.

4.2.2.2 - 4.2.2.5
	Log server configuration should be configured by hand to prevent misconfiguration.

5.2.15 Ensure\ SSH\ access\ is\ limited
	Automated configuration may lockout administration.

5.3.2
	pam_faillock.so configuration should be configured by hand to prevent misconfiguration.

6.1.10 - 6.1.12
	File/folder permissions/existence should be updated manually.

6.2.1 Ensure\ password\ fields\ are\ not\ empty
	Passwords must be set manually.

6.2.5
	Proper UID 0 account may have been remnamed, should be completed manually.

6.2.6
	root PATH must be set by end user.

6.2.7 - 6.2.14
	User file/folder permissions/existence should be updated manually.

6.2.15 Ensure\ all\ groups\ in\ /etc/passwd\ exist\ in\ /etc/group
	Account modifaction or group creation should be completed manually.

6.2.16 - 6.2.17
	Duplicate IDs must be remediated by end user.

6.2.18 - 6.2.19
	Duplicate names must be remediated by end user.
