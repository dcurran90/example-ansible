#!/usr/bin/env bash
#check vmware tools is installed and up to date for *nix

#pull latest version from ansible var
vmware_current=$1

#check vmware tools
if [ ! -f /usr/bin/vmware-toolbox-cmd ]; then
    echo "vmwaretools needs to be instaleld"
#copy vmware tools tar file from ansible/packer master
    cd /tmp/
    tar -zxf VMwareTools-$vmware_current.tar.gz
    chmod +x vmware-tools-distrib/vmware-install.pl
    vmware-tools-distrib/vmware-install.pl -d default
#check it's installed
    install_check=$(/usr/bin/vmware-toolbox-cmd --version | awk '{print $1}');
    if [ -z "$install_check" ]; then
        echo "looks like vmware tools has not installed"
        exit 1
    else
        echo "vmware tools version $install_check successfully installed"
    fi
#cleanup
    cd
    rm -f /tmp/VMwareTools-$vmware_current.tar.gz
    rm -f /tmp/vmware-uninstall-tools.pl
#check vmware tools is the latest version
elif [ -f /usr/bin/vmware-toolbox-cmd ]; then
    version_check=$(/usr/bin/vmware-toolbox-cmd --version | awk '{print $1}');
    if [ "$version_check" != "$vmware_current" ]; then
        echo "need to update vmware tools"
        chmod +x /tmp/vmware-uninstall-tools.pl
        /tmp/vmware-uninstall-tools.pl -d default
        echo "uninstalled vmware tools"
        echo "vmwaretools needs to be instaleld"
#copy vmware tools tar file from ansible/packer master
        cd /tmp/
        tar -zxf VMwareTools-$vmware_current.tar.gz
        chmod +x vmware-tools-distrib/vmware-install.pl
        vmware-tools-distrib/vmware-install.pl -d default
#check it's installed
        install_check=$(/usr/bin/vmware-toolbox-cmd --version);
        if [ -z "$install_check" ]; then
            echo "looks like vmware tools has not installed"
            exit 1
        else
            echo "vmware tools version $install_check successfully installed"
        fi
#cleanup
        cd
        rm -f /tmp/VMwareTools-$vmware_current.tar.gz
        rm -f /tmp/vmware-uninstall-tools.pl
    else
        echo "vmware tools looks to be the latest version. skipping"
        rm -f /tmp/VMwareTools-$vmware_current.tar.gz
        rm -f /tmp/vmware-uninstall-tools.pl
    fi
fi

